package ua.ithillel.hillelprospring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ua.ithillel.hillelprospring.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Transactional
    @Modifying
    @Query("update User u set u.name = ?1 where u.id = ?2")
    Integer update(String name, Integer id);

    User getUserByNameAndSurname(String name, String surname);
}
