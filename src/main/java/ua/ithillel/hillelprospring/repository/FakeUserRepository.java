package ua.ithillel.hillelprospring.repository;

import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Repository;
import ua.ithillel.hillelprospring.entity.User;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FakeUserRepository {
    private final List<User> users = new ArrayList<>();

    @PostConstruct
    public void init() {
        users.add(new User(0,"Alex", "Alexov", 20, "email@email.com"));
        users.add(new User(1,"Bob", "Bobov", 25, "email@email.com"));
        users.add(new User(2,"Sveta", "Or", 24, "email@email.com"));

    }

    public List<User> getAll() {
        return users;
    }

    public User getById(Integer id) {
        return users.get(id);
    }

    public User getByIdAndName(String name, String surname) {
        //logic validation...
        return null;
    }

    public User save(User user) {
        users.add(user);
        user.setId(users.size());
        return user;
    }

    public User update(Integer id, User user) {
        final User oldUser = users.get(id);
        oldUser.setName(user.getName());
        //update fields....
        return users.get(id);
    }

    public Integer delete(int id) {
        users.remove(id);
        return id;
    }
}
