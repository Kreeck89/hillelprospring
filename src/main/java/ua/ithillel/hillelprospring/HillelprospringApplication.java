package ua.ithillel.hillelprospring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ua.ithillel.hillelprospring.configuration.MyBeanConfiguration;
import ua.ithillel.hillelprospring.controller.ActionController;
import ua.ithillel.hillelprospring.controller.UserController;
import ua.ithillel.hillelprospring.entity.User;

import java.util.List;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class HillelprospringApplication {

    public static void main(String[] args) {
        SpringApplication.run(HillelprospringApplication.class, args);

//        final ConfigurableApplicationContext context = SpringApplication.run(HillelprospringApplication.class, args);
//        final UserController userController = context.getBean(UserController.class);
//        final List<User> all = userController.getAll();
//        all.forEach(System.out::println);
//
//        final ActionController actionController = context.getBean(ActionController.class);
//        actionController.actionOne();

//        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyBeanConfiguration.class);
//        final String stringBean = context.getBean("stringBean", String.class);
//        final User customUser = context.getBean("getCustomUser", User.class);
//        customUser.setAge(1000);
//        final User customUser2 = context.getBean("getCustomUser", User.class);
//        System.out.println(stringBean);
//        System.out.println(customUser);
//        System.out.println(customUser2);
//
//        final String queueName = context.getBean("getQueueName", String.class);
//        System.out.println(queueName);
    }
}
