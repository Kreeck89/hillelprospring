package ua.ithillel.hillelprospring.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import ua.ithillel.hillelprospring.entity.User;

@Configuration
@ComponentScan("ua.ithillel.hillelprospring.configuration")
@PropertySource("classpath:application.properties")
public class MyBeanConfiguration {

    @Value("${queue.name}")
    private String queueName;

    @Bean
    public String stringBean() {
        return "bean configuration string from MyBeanConfiguration.";
    }

    @Bean
    @Scope("prototype")
    public User getCustomUser() {
        return new User(1,"Bob", "Ivankov", 20, "email@email.com");
    }

    @Bean
    public String getQueueName() {
        return queueName;
    }
}
