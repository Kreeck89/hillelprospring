package ua.ithillel.hillelprospring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ithillel.hillelprospring.entity.User;
import ua.ithillel.hillelprospring.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

//    private final FakeUserRepository fakeUserRepository;
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User getById(Integer id) {
        return userRepository.findById(id).orElseThrow();
    }

    public Optional<User> getOptionalById(Integer id) {
        return userRepository.findById(id);
    }

    public User getByNameAndSurname(String name, String surname) {
        return userRepository.getUserByNameAndSurname(name, surname);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User update(User user) {
        return userRepository.save(user);
    }

    public Integer updateNameById(String name, Integer id) {
        return userRepository.update(name, id);
    }

    public Integer delete(Integer id) {
        userRepository.deleteById(id);
        return id;
    }
}
