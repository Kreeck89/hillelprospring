package ua.ithillel.hillelprospring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.ithillel.hillelprospring.controller.dto.IntegerDto;
import ua.ithillel.hillelprospring.controller.dto.StringDto;
import ua.ithillel.hillelprospring.controller.dto.UserDto;
import ua.ithillel.hillelprospring.controller.mapper.UserMapper;
import ua.ithillel.hillelprospring.entity.User;
import ua.ithillel.hillelprospring.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public UserController(final UserService userService, final UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    //    @RequestMapping(path = "/all", method = RequestMethod.GET)
    @GetMapping
    public ResponseEntity<List<UserDto>> getAll() {
        return new ResponseEntity<>(
                userService.getAll().stream()
                           .map(userMapper::toDto)
                           .collect(Collectors.toList()),
                HttpStatus.OK
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getById(@PathVariable Integer id) {
        final User userById = userService.getById(id);
        return ResponseEntity.ok(userMapper.toDto(userById));
    }

    @GetMapping("/{name}/{surname}")
    public ResponseEntity<UserDto> getByNameAndSurname(
            @PathVariable String name,
            @PathVariable String surname
    ) {
        return ResponseEntity.ok(userMapper.toDto(userService.getByNameAndSurname(name, surname)));
    }

    @GetMapping("/filter")
    public UserDto getUserByFilter(
            @RequestParam Integer first,
            @RequestParam(required = false) String secondName,
            @RequestParam(required = false) String thirdName
            ) {
        return null;
    }

    @GetMapping("/email/{userId}")
    public StringDto getEmailById(@PathVariable Integer userId) {
        return new StringDto("alex@mail.ua");
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> save(@RequestBody UserDto userDto) {
        if (userDto.getSurname() == null || userDto.getSurname().isBlank()) {
            return ResponseEntity.badRequest().body(null);
        }

        return new ResponseEntity<>(
                userMapper.toDto(
                        userService.save(
                                userMapper.toEntity(userDto))),
                HttpStatus.CREATED
        );
    }

    @PutMapping
    public UserDto update(@RequestBody UserDto userDto) {
        return userMapper.toDto(
                userService.update(userMapper.toEntity(userDto)));
    }

    @PutMapping("/{id}/{name}")
    public IntegerDto updateNameById(
            @PathVariable Integer id,
            @PathVariable String name
    ) {
        return new IntegerDto(userService.updateNameById(name, id));
    }

    @DeleteMapping
    public Integer delete(@RequestParam Integer id) {
        return userService.delete(id);
    }
}
