package ua.ithillel.hillelprospring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import ua.ithillel.hillelprospring.service.Action;

@Controller
public class ActionController {

    private final Action action;

    @Value("${aws.timeout.for.my.service}")
    private Integer awsTimeout;

    private final String stringBean;

    @Autowired
    public ActionController(@Qualifier("actionOneService") Action action, final String stringBean) {
        this.action = action;
        this.stringBean = stringBean;
    }

    public void actionOne() {
        System.out.println(stringBean);
        System.out.println(awsTimeout);
    }
}
